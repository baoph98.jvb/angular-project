import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DemoRoutingModule } from './demo.routing';
import { DemoOneComponent } from './demo-one/demo-one.component';
import { MaterialModule } from '../../shared/module/material/material.module';
import { FilterListComponent } from './demo-one/filter-list/filter-list.component';
import { DisplayListComponent } from './demo-one/display-list/display-list.component';
import { ActivatedListComponent } from './demo-one/activated-list/activated-list.component';
import { DemoTwoComponent } from './demo-two/demo-two.component';
import { CounterComponent } from './demo-two/counter/counter.component';
import { FormsModule } from '@angular/forms';
import { NavigationBarComponent } from '../../shared/layout/navigation-bar/navigation-bar.component';
import { DemoThreeComponent } from './demo-three/demo-three.component';
import { HomeComponent } from './demo-three/home/home.component';
import { ProfileComponent } from './demo-three/profile/profile.component';
import { DemoFourComponent } from './demo-four/demo-four.component';
import { AboutComponent } from './demo-four/about/about.component';
import { CounterService } from './demo-two/counter.service';


@NgModule({
  declarations: [
    NavigationBarComponent,
    DemoOneComponent,
    FilterListComponent,
    DisplayListComponent,
    ActivatedListComponent,
    DemoTwoComponent,
    CounterComponent,
    DemoThreeComponent,
    HomeComponent,
    ProfileComponent,
    DemoFourComponent,
    AboutComponent,
  ],
  imports: [
    CommonModule,
    MaterialModule,
    DemoRoutingModule,
    FormsModule,
  ],
  exports: [
    NavigationBarComponent,
  ],
  providers: [
    CounterService,
  ],
})
export class DemoModule {
}
