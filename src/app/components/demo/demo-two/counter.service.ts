import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class CounterService {

  counter = new BehaviorSubject(0);
  clicked = new BehaviorSubject(0);

  abc = 0;

  constructor() {
  }

  addCounter(value: string) {
    const numberAdd = Number(value);
    this.abc = numberAdd;
    this.counter.next(this.counter.value + numberAdd);
    this.clicked.next(this.clicked.value + 1);
  }
}
