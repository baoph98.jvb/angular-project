import { Component, OnInit } from '@angular/core';
import { CounterService } from '../counter.service';

@Component({
  selector: 'app-counter',
  templateUrl: './counter.component.html',
  styleUrls: ['./counter.component.scss'],
})
export class CounterComponent implements OnInit {

  counter = '0';

  constructor(
    private counterService: CounterService
  ) {
  }

  ngOnInit(): void {
  }

  counting() {
    console.log(this.counter);
    this.counterService.addCounter(this.counter);
  }
}
