import { Component, OnInit } from '@angular/core';
import { CounterService } from './counter.service';
import { DemoService } from '../../../shared/services/demo/demo.service';

@Component({
  selector: 'app-demo-two',
  templateUrl: './demo-two.component.html',
  styleUrls: ['./demo-two.component.scss'],
})
export class DemoTwoComponent implements OnInit {

  counted = 0;
  clicked = 0;
  abc = 0;

  constructor(
    private counterService: CounterService,
    private demoService: DemoService
  ) {
    this.counterService.counter.subscribe(
      value => this.counted = value,
    );
    this.counterService.clicked.subscribe(
      value => this.clicked = value,
    );
    this.abc = this.counterService.abc;
  }

  ngOnInit(): void {
  }

}
