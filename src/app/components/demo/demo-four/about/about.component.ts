import { AfterViewInit, Component, ElementRef, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss'],
})
export class AboutComponent implements OnInit, AfterViewInit, OnDestroy {

  localValue = 'from Local';

  @Input()
  fromParent;

  @ViewChild('retrieveDom')
  retrieveDom: ElementRef;

  constructor() {
    console.log('---------------');
    console.log('constructor is called');
    console.log(this.localValue);
    console.log(this.fromParent);
    console.log(this.retrieveDom);
  }

  ngOnInit(): void {
    console.log('---------------');
    console.log('ngOnInit is called');
    console.log(this.localValue);
    console.log(this.fromParent);
    console.log(this.retrieveDom);
  }

  ngAfterViewInit(): void {
    console.log('---------------');
    console.log('ngAfterViewInit is called');
    console.log(this.localValue);
    console.log(this.fromParent);
    console.log(this.retrieveDom);
  }

  ngOnDestroy(): void {
    console.log('---------------');
    console.log('ngOnDestroy is called');
    console.log(this.localValue);
    console.log(this.fromParent);
    console.log(this.retrieveDom);
  }
}
