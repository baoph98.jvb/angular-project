import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-activated-list',
  templateUrl: './activated-list.component.html',
  styleUrls: ['./activated-list.component.scss'],
})
export class ActivatedListComponent implements OnInit {

  @Input()
  user: { id: number, avatar: string };

  constructor() {
  }

  ngOnInit(): void {
  }

}
