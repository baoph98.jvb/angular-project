import { Component, OnInit } from '@angular/core';
import { DemoService } from '../../../shared/services/demo/demo.service';
import { CounterService } from '../demo-two/counter.service';

@Component({
  selector: 'app-demo-one',
  templateUrl: './demo-one.component.html',
  styleUrls: ['./demo-one.component.scss'],
})
export class DemoOneComponent implements OnInit {

  isBorderOn = false;

  filterMode = 1;
  userSelected = null;
  abc = 0;

  constructor(
    private demoService: DemoService,
    private counterService: CounterService
  ) {
    this.demoService.statusBorderComponent
      .subscribe(value => this.isBorderOn = value);
    this.abc = this.counterService.abc;
  }

  ngOnInit(): void {
  }

  displayBorder() {
    this.demoService.toggleBorder(!this.isBorderOn);
  }

  filterChanged(mode: number) {
    this.filterMode = mode;
  }

  idChanged(user: { id: number; avatar: string }) {
    this.userSelected = user;
  }
}
