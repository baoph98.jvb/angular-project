import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { MatRadioChange } from '@angular/material/radio';

@Component({
  selector: 'app-filter-list',
  templateUrl: './filter-list.component.html',
  styleUrls: ['./filter-list.component.scss'],
})
export class FilterListComponent implements OnInit {

  @Output()
  filterMode: EventEmitter<number> = new EventEmitter<number>();
  filterModeLocal = 1;

  constructor() {
  }

  ngOnInit(): void {
  }

  filterChange($event: MatRadioChange) {
    this.filterModeLocal = Number($event.value);
    this.filterMode.emit(this.filterModeLocal);
  }
}
