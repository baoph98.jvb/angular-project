import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';

@Component({
  selector: 'app-display-list',
  templateUrl: './display-list.component.html',
  styleUrls: ['./display-list.component.scss'],
})
export class DisplayListComponent implements OnChanges {
  @Input()
  filterMode: number;

  list: MockUpDataUser[] = [];

  displayedColumns: string[] = ['id', 'avatar'];

  @Output()
  selectedIDEmit: EventEmitter<MockUpDataUser> = new EventEmitter();
  selectedID: number;

  constructor(
    private http: HttpClient,
  ) {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.filterMode && changes.filterMode.currentValue !== undefined) {
      this.queue(this.filterMode);
    }
  }

  queue(mode = 1) {
    const queryMode = this.filterModeQuery(mode);
    // https://angular.io/guide/http#url-query-strings
    this.http.get<MockUpDataUser[]>(`http://localhost:3000/people${queryMode}`).pipe(
      debounceTime(2000),
      distinctUntilChanged(),
      map(lr => {
        if (mode === 3) {
          return lr.filter(l => l.id % 2 === 0);
        } else if (mode === 4) {
          return lr.filter(l => l.id % 2 !== 0);
        }
        return lr;
      }),
    ).subscribe(value => this.list = value);
  }

  click(id: number, avatar: string) {
    this.selectedID = id;
    const passingOb = {
      id: this.selectedID,
      avatar,
    };
    console.log(passingOb);
    this.selectedIDEmit.emit(passingOb);
  }

  private filterModeQuery(mode: number) {
    switch (mode) {
      case 2:
        return `?_sort=id&_order=desc`;
      case 3:
        return ``;
      case 4:
        return ``;
      default:
        return `?_sort=id&_order=asc`;
    }
  }
}

export interface MockUpDataUser {
  id: number;
  avatar: string;
}
