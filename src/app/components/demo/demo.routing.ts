import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DemoOneComponent } from './demo-one/demo-one.component';
import { DemoTwoComponent } from './demo-two/demo-two.component';
import { DemoThreeComponent } from './demo-three/demo-three.component';
import { HomeComponent } from './demo-three/home/home.component';
import { ProfileComponent } from './demo-three/profile/profile.component';
import { DemoFourComponent } from './demo-four/demo-four.component';


const routes: Routes = [
  { path: 'demo-1', component: DemoOneComponent },
  { path: 'demo-2', component: DemoTwoComponent },
  {
    path: 'demo-3', component: DemoThreeComponent, children: [
      { path: 'home', component: HomeComponent, pathMatch: 'full' },
      { path: 'profile', component: ProfileComponent, pathMatch: 'full' },
    ],
  },
  { path: 'demo-4', component: DemoFourComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DemoRoutingModule {
}
