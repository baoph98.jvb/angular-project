import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { WelcomeComponent } from './components/welcome/welcome.component';
import { StaffListComponent } from './components/staff-list/staff-list.component';


const routes: Routes = [
  { path: 'welcome', component: WelcomeComponent },
  { path: 'staff-list', component: StaffListComponent },
  // if there is no match of above routes and URL is '' then redirect to /build
  { path: '', redirectTo: 'welcome', pathMatch: 'full' },
  // wildcard router
  // { path: '**', component: NotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
