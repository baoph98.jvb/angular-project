export class IProduct {
  productId: number;
  price: number;
  starRating: number;
  imageUrl: string;

  [key: string]: any;
}
