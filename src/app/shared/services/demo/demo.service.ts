import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class DemoService {

  public statusBorderComponent: Observable<boolean>;
  private displayBorderComponent: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor() {
    this.statusBorderComponent = this.displayBorderComponent.asObservable();
  }

  toggleBorder(val: boolean) {
    this.displayBorderComponent.next(val);
  }
}
