import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app.routing';
import { AppComponent } from './app.component';
import { WelcomeComponent } from './components/welcome/welcome.component';
import { StarComponent } from './shared/components/star/star.component';
import { StaffListComponent } from './components/staff-list/staff-list.component';
import { StaffDetailComponent } from './components/staff-list/staff-detail/staff-detail.component';
import { MaterialModule } from './shared/module/material/material.module';
import { DemoService } from './shared/services/demo/demo.service';
import { DemoModule } from './components/demo/demo.module';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    WelcomeComponent,
    StarComponent,
    StaffListComponent,
    StaffDetailComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    MaterialModule,
    DemoModule,
    AppRoutingModule,
  ],
  providers: [
    // Demo Service
    DemoService,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {
}
